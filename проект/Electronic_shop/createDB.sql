create database Electronic_shop
go

use Electronic_shop
go

create  table ������ 
(
ID_������ int primary key identity(1,1),
������������ varchar(40) not null,
���� money not null
)
go

create  table ������� 
(
ID_������� int primary key identity(1,1),
������� varchar(20) not null,
��� varchar(20) not null,
�������� varchar(20) not null,
����� varchar(20) not null,
������ varchar(20) not null,
������� varchar(20) not null,
�����_����� varchar(50) not null,
)
go

create table ���������
(
ID_��������� int primary key identity(1,1),
������������ varchar(40) not null,
)
go

create table ����������
(
ID_���������� int primary key identity(1,1),
������� varchar(20) not null,
��� varchar(20) not null,
�������� varchar(20) not null,
����� varchar(20) not null,
������ varchar(20) not null,
����� varchar(20) not null,
����� varchar(20) not null,
������� varchar(20) not null,
��������_����� varchar(20) not null,
ID_��������� int foreign key references ���������(ID_���������)
)
go

create table �������_������
(
ID_������� int primary key identity(1,1),
������������ varchar(20)
)
go

create  table ������_��_������������
(
ID_������ int primary key identity(1,1),
ID_������ int foreign key references ������ (ID_������),
ID_������� int foreign key references �������(ID_�������),
��������_�������� varchar(100) not null,
ID_���������� int foreign key references ����������(ID_����������),
������ int foreign key references �������_������ (ID_�������),
�����_��_������ money,
����_������ datetime default getdate()
)
go

create table ������
(
ID_������ int primary key identity(1,1),
ID_���������� int foreign key references ����������(ID_����������),
����������_�����������_����� int,
������� money,
����_������ date
)
go
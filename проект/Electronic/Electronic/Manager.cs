﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace Electronic
{
    public partial class Manager : Form
    {
        public Manager()
        {
            InitializeComponent();
        }
        SqlConnection sqlcon = new SqlConnection();
        DataSet dataset = new DataSet();
        SqlDataAdapter adapter = new SqlDataAdapter();
        SqlCommandBuilder builder = new SqlCommandBuilder();

        DataBase database = new DataBase();
        private void Manager_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void button1_MouseMove(object sender, MouseEventArgs e)
        {
            button1.BackColor = Color.Black;
            button1.ForeColor = Color.White;
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            button1.BackColor = Color.White;
            button1.ForeColor = Color.Black;
        }

        private void button3_MouseMove(object sender, MouseEventArgs e)
        {
            button3.BackColor = Color.Black;
            button3.ForeColor = Color.White;
        }

        private void button3_MouseLeave(object sender, EventArgs e)
        {
            button3.BackColor = Color.White;
            button3.ForeColor = Color.Black;
        }

        private void button2_MouseMove(object sender, MouseEventArgs e)
        {
            button2.BackColor = Color.Black;
            button2.ForeColor = Color.White;
        }

        private void button2_MouseLeave(object sender, EventArgs e)
        {
            button2.BackColor = Color.White;
            button2.ForeColor = Color.Black;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Auth form1 = new Auth();
            this.Hide();
            form1.Show();
        }

        private void Manager_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = database.getData("checkRequests");
            dataGridView2.DataSource = database.getData("Сотрудники where ID_Должности = 3");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(database.connectDataBase()))
            {

                try
                {
                    DialogResult result;
                    result = MessageBox.Show("Нажмите ОК для завершения действия", "Подтверждение действия", MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);
                    if (result == System.Windows.Forms.DialogResult.OK)
                    {
                        if (textBox1.Text.Replace(" ", "") != "")
                        {
                            if (textBox2.Text.Replace(" ", "") != "")
                            {
                                string update = "update Заявки_на_обслуживание set ID_Сотрудника =" + textBox2.Text + " where ID_Заявки =" + textBox1.Text
                                    + "update Заявки_на_обслуживание set Статус = 2 where ID_Заявки = " + textBox1.Text;
                                adapter = new SqlDataAdapter(update, connection);
                                dataset = new DataSet();
                                adapter.Fill(dataset, "Заявки_на_обслуживание");
                                MessageBox.Show("Работа назначена!", "Назначение работы", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                dataGridView1.DataSource = database.getData("Заявки_на_обслуживание");
                            }
                            else
                            {
                                MessageBox.Show("ID не может быть пустой", "Назначение работы", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {
                            MessageBox.Show("ID не может быть пустой", "Назначение работы", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message.ToString());
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(database.connectDataBase()))
            {
                try
                {
                    DialogResult result;
                    result = MessageBox.Show("Нажмите ОК для завершения действия", "Подтверждение действия", MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);
                    if (result == System.Windows.Forms.DialogResult.OK)
                    {
                        if (textBox4.Text.Replace(" ", "") != "")
                        {
                            string update = "update Заявки_на_обслуживание set Статус = 3 where ID_Заявки =" + textBox4.Text;
                            adapter = new SqlDataAdapter(update, connection);
                            dataset = new DataSet();
                            adapter.Fill(dataset, "Заявки_на_обслуживание");
                            MessageBox.Show("Услуга завершена!", "Добавление услуги", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            dataGridView1.DataSource = database.getData("Заявки_на_обслуживание");
                        }
                        else
                        {
                            MessageBox.Show("ID не может быть пустой", "Завершение работы", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message.ToString());
                }
            }
        }
    }
}

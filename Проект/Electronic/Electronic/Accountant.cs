﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace Electronic
{
    public partial class Accountant : Form
    {
        public Accountant()
        {
            InitializeComponent();
        }
        DataBase database = new DataBase();
        SqlConnection sqlcon = new SqlConnection();
        DataSet dataset = new DataSet();
        SqlDataAdapter adapter = new SqlDataAdapter();
        SqlCommandBuilder builder = new SqlCommandBuilder();
        private void Accountant_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = database.getData("Отчёты");
        }

        private void Accountant_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void button1_MouseMove(object sender, MouseEventArgs e)
        {
            button1.BackColor = Color.Black;
            button1.ForeColor = Color.White;    
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            button1.BackColor = Color.White;
            button1.ForeColor = Color.Black;
        }

        private void button2_MouseMove(object sender, MouseEventArgs e)
        {
            button2.BackColor = Color.Black;
            button2.ForeColor = Color.White;
        }

        private void button2_MouseLeave(object sender, EventArgs e)
        {
            button2.BackColor = Color.White;
            button2.ForeColor = Color.Black;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(database.connectDataBase()))
            {
                try
                {
                    if (maskedTextBox1.Text.Replace(" ", "") != "")
                    {
                            string query =  "insert into Отчёты (ID_Сотрудника,Дата_отчёта) values " 
                            + "(" + @"'" + "4" + @"'" +"," + @"'" + maskedTextBox1.Text + @"'"  + ")";
                            adapter = new SqlDataAdapter(query, connection);
                            dataset = new DataSet();
                            adapter.Fill(dataset, "Услуги");
                            MessageBox.Show("Отчёт создан!", "Создание отчёта", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("Дата не может быть пустой", "Создание отчёта", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        adapter = new SqlDataAdapter("DBCC CHECKIDENT (Услуги,RESEED,(select Count(ID_Услуги) - 1)", connection);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message.ToString());
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Auth form1 = new Auth();
            this.Hide();
            form1.Show();
        }
    }
}

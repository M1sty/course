﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace Electronic
{
    public partial class AddService : Form
    {
        public AddService()
        {
            InitializeComponent();
        }
        SqlConnection sqlcon = new SqlConnection();
        DataSet dataset = new DataSet();
        SqlDataAdapter adapter = new SqlDataAdapter();
        SqlCommandBuilder builder = new SqlCommandBuilder();
        Director director = new Director();
        DataBase database = new DataBase();
        private void button1_MouseMove(object sender, MouseEventArgs e)
        {
            button1.BackColor = Color.Black;
            button1.ForeColor = Color.White;
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            button1.BackColor = Color.White;
            button1.ForeColor = Color.Black;
        }

        private void button2_MouseMove(object sender, MouseEventArgs e)
        {
            button2.BackColor = Color.Black;
            button2.ForeColor = Color.White;
        }

        private void button2_MouseLeave(object sender, EventArgs e)
        {
            button2.BackColor = Color.White;
            button2.ForeColor = Color.Black;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            director.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(database.connectDataBase()))
            {
                try
                {
                    if(textBox1.Text.Replace(" ", "") != "")
                    {
                        if (maskedTextBox1.Text.Replace(" ","") !=  "")
                        {
                            string reg = "insert into Услуги(Наименование,Цена) values" +
                            "(" + @"'" + textBox1.Text + @"'" + "," + @"'" + maskedTextBox1.Text + @"'" + ")";
                            adapter = new SqlDataAdapter(reg, connection);
                            dataset = new DataSet();
                            adapter.Fill(dataset, "Услуги");
                            MessageBox.Show("Услуга добавлена!", "Добавление услуги", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Hide();
                            director.Show();
                        }
                        else
                        {
                            MessageBox.Show("Цена не может быть пустой", "Добавление услуги", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            adapter = new SqlDataAdapter("DBCC CHECKIDENT (Услуги,RESEED    ,(select Count(ID_Услуги) - 1)", connection);
                        }
                    } 
                    else
                    {
                        MessageBox.Show("Наименование не может быть пустым", "Добавление услуги", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        adapter = new SqlDataAdapter("DBCC CHECKIDENT (Услуги,RESEED,(select Count(ID_Услуги) - 1)", connection);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message.ToString());
                }
            }
        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void AddService_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}

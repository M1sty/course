﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace Electronic
{
    public partial class Registration : Form
    {
        DataBase database = new DataBase();
        SqlConnection sqlcon = new SqlConnection();
        SqlDataAdapter adapter = new SqlDataAdapter();
        SqlCommandBuilder builder = new SqlCommandBuilder();
        DataSet dataset = new DataSet();
        Auth form1 = new Auth();
        public Registration()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(database.connectDataBase()))
            {
                try
                {
                    string reg = "insert into Клиенты (Фамилия,Имя,Отчество,Логин,Пароль,Телефон,Адрес_почты) values" +
                        "("
                        + @"'" + textBox1.Text + @"'" + "," + @"'" + textBox2.Text + @"'" + "," + @"'" + textBox3.Text + @"'" + "," +
                         @"'" + textBox4.Text + @"'" + "," + @"'" + textBox5.Text + @"'" + "," + @"'" + maskedTextBox1.Text + @"'" + "," 
                          + @"'" + textBox6.Text + comboBox1.Text +@"'"
                            + ")";         
                    adapter = new SqlDataAdapter(reg, connection);
                    dataset = new DataSet();
                    adapter.Fill(dataset,"Клиенты");
                    MessageBox.Show("Вы успешно зарегистрировались!" + Environment.NewLine + "Ваш логин:" + textBox4.Text + Environment.NewLine + "Ваш пароль:" + textBox5.Text);
                    this.Hide();
                    form1.Show();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message.ToString());
                }
            }
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            button1.BackColor = Color.White;
            button1.ForeColor = Color.Black;
        }

        private void button1_MouseEnter(object sender, EventArgs e)
        {
            button1.BackColor = Color.Black;
            button1.ForeColor = Color.White;
        }

        private void Registration_Load(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex= 0;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            form1.Show();
        }

        private void button2_MouseMove(object sender, MouseEventArgs e)
        {
            button2.BackColor = Color.Black;
            button2.ForeColor = Color.White;
        }

        private void button2_MouseLeave(object sender, EventArgs e)
        {
            button2.BackColor = Color.White;
            button2.ForeColor = Color.Black;
        }

        private void Registration_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}

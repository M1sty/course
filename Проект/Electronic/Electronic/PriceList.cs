﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace Electronic
{

    public partial class PriceList : Form
    {
        public string ID_Client;
        SqlConnection sqlcon = new SqlConnection();
        DataSet dataset = new DataSet();
        SqlDataAdapter adapter = new SqlDataAdapter();
        SqlCommandBuilder builder = new SqlCommandBuilder();
        DataBase database = new DataBase();
        Client client = new Client();
        public PriceList()
        {
            InitializeComponent();
        }

        private void button1_MouseMove(object sender, MouseEventArgs e)
        {
            button1.BackColor = Color.Black;
            button1.ForeColor = Color.White;
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            button1.BackColor = Color.White;
            button1.ForeColor = Color.Black;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(database.connectDataBase()))
            {
                try
                {
                    if (textBox2.Text.Replace(" ","") != "")
                    {
                        string reg = "insert into Заявки_на_обслуживание (ID_Услуги,ID_Клиента,Описание_проблемы,Статус) values" +
                        "(" + @"'" + textBox1.Text + @"'" + "," + @"'" + ID_Client + @"'" + "," + @"'" + textBox2.Text + @"'" + "," + @"'" + "1" + @"'"
                        + ")";
                        adapter = new SqlDataAdapter(reg, connection);
                        dataset = new DataSet();
                        adapter.Fill(dataset, "Сотрудники");
                        MessageBox.Show("Заявка успешно создана!", "Отправка заявки", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("Описание проблемы не может быть пустым!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message.ToString());
                }
            }
        }

        private void PriceList_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = database.getData("Услуги");
        }

        private void PriceList_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
    }
}

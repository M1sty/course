﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace Electronic
{
    public partial class deleteEmployee : Form
    {
        public deleteEmployee()
        {
            InitializeComponent();
        }
        DataBase database = new DataBase();
        Director director = new Director();
        SqlConnection sqlcon = new SqlConnection();
        DataSet dataset = new DataSet();
        SqlDataAdapter adapter = new SqlDataAdapter();
        SqlCommandBuilder builder = new SqlCommandBuilder();
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult dialogResult;
                dialogResult = MessageBox.Show("Вы точно хотите удалить сотрудника?", "Удаление сотрудника", MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);
                if (dialogResult == DialogResult.OK)
                {
                        database.deleteEmployee("Заявки_на_обслуживание", textBox1.Text);
                        database.deleteEmployee("Отчёты", textBox1.Text);
                        database.deleteEmployee("Сотрудники", textBox1.Text);
                        MessageBox.Show("Сотрудник удален!", "Удаление сотрудника", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dataGridView1.DataSource = database.getData("Сотрудники");
                    
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           
        }

        private void button1_MouseMove(object sender, MouseEventArgs e)
        {
            button1.BackColor = Color.Black;
            button1.ForeColor = Color.White;
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            button1.BackColor = Color.White;
            button1.ForeColor = Color.Black;
        }

        private void deleteEmployee_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = database.getData("Сотрудники");
        }

        private void button2_MouseMove(object sender, MouseEventArgs e)
        {
            button2.BackColor = Color.Black;
            button2.ForeColor = Color.White;
        }

        private void button2_MouseLeave(object sender, EventArgs e)
        {
            button2.BackColor = Color.White;
            button2.ForeColor = Color.Black;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            director.Show();
        }

        private void deleteEmployee_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}

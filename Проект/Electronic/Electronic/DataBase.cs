﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
namespace Electronic
{
    class DataBase
    {
       
        SqlConnection sqlcon = new SqlConnection();
        DataSet dataset = new DataSet();
        SqlDataAdapter adapter = new SqlDataAdapter();
        SqlCommandBuilder builder = new SqlCommandBuilder();
        public object getData(string tableName)
        {
            string query = "select * from " + tableName;
            using (SqlConnection connection = new SqlConnection(connectDataBase()))
            {
                adapter = new SqlDataAdapter(query, connection);
                builder = new SqlCommandBuilder(adapter);
                dataset = new DataSet();
                adapter.Fill(dataset);
                
                return dataset.Tables[0];
            }   
        }
        public object deleteEmployee(string tableName,string id)
        {
            string query = "delete from " + tableName + " where ID_Сотрудника =" + id;
            using (SqlConnection connection = new SqlConnection(connectDataBase()))
            {
                adapter = new SqlDataAdapter(query, connection);
                builder = new SqlCommandBuilder(adapter);
                dataset = new DataSet();
                adapter.Fill(dataset);
            
                return null;
            }
        }
        public object deleteService(string tableName, string id)
        {
            string query = "delete from " + tableName + " where ID_Услуги =" + id;
            using (SqlConnection connection = new SqlConnection(connectDataBase()))
            {
                adapter = new SqlDataAdapter(query, connection);
                builder = new SqlCommandBuilder(adapter);
                dataset = new DataSet();
                adapter.Fill(dataset);
               
                return null;
            }
        }
        public string connectDataBase()
        {
            return @"Data Source=COMPUTER\GOODDEVELOPER; Initial Catalog = Electronic_shop; Integrated Security=true";
        }
    }
}

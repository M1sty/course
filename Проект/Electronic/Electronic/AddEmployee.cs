﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace Electronic
{
    public partial class AddEmployee : Form
    {

        SqlConnection sqlcon = new SqlConnection();
        DataSet dataset = new DataSet();
        SqlDataAdapter adapter = new SqlDataAdapter();
        SqlCommandBuilder builder = new SqlCommandBuilder();
        Director director = new Director();
        public AddEmployee()
        {
            InitializeComponent();
        }
        DataBase database = new DataBase();
        private void AddEmployee_Load(object sender, EventArgs e)
        {
            metroComboBox1.SelectedIndex = 0;
        }

        private void button1_MouseMove(object sender, MouseEventArgs e)
        {
            button1.BackColor = Color.Black;
            button1.ForeColor = Color.White;
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            button1.BackColor = Color.White;
            button1.ForeColor = Color.Black;
        }
        private void button2_MouseMove(object sender, MouseEventArgs e)
        {
            button2.BackColor = Color.Black;
            button2.ForeColor = Color.White;
        }

        private void button2_MouseLeave(object sender, EventArgs e)
        {
            button2.BackColor = Color.White;
            button2.ForeColor = Color.Black;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            using (SqlConnection connection = new SqlConnection(database.connectDataBase()))
            {
                try
                {
                    if (metroComboBox1.SelectedIndex != 0)
                    {
                        if (metroComboBox1.SelectedIndex != 1)
                        {
                            string reg = "insert into Сотрудники (Фамилия,Имя,Отчество,Логин,Пароль,Серия,Номер,Телефон,Домашний_адрес,ID_Должности) values" +
                       "("
                       + @"'" + textBox1.Text + @"'" + "," + @"'" + textBox2.Text + @"'" + "," + @"'" + textBox3.Text + @"'" + "," +
                        @"'" + textBox4.Text + @"'" + "," + @"'" + textBox5.Text + @"'" + "," + @"'" + maskedTextBox1.Text + @"'" + ","
                         + @"'" + maskedTextBox2.Text + @"'" + "," + @"'" + maskedTextBox3.Text + @"'" + "," + @"'" + textBox6.Text + @"'" + "," +
                         @"'" + metroComboBox1.SelectedIndex.ToString() + @"'"
                           + ")";
                            adapter = new SqlDataAdapter(reg, connection);
                            dataset = new DataSet();
                            adapter.Fill(dataset, "Сотрудники");
                            MessageBox.Show("Сотрудник добавлен!", "Добавление сотрудника", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            this.Hide();
                            director.Show();
                        }
                        else
                        {
                            MessageBox.Show("Директор должен быть только один", "Добавление сотрудника", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            adapter = new SqlDataAdapter("DBCC CHECKIDENT (Сотрудники,RESEED,(select Count(ID_Сотрудника) - 1)", connection);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Выберите должность", "Добавление сотрудника", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message.ToString());
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
         
            director.Show();
        }

        private void AddEmployee_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}

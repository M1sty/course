﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using MetroFramework.Components;
using MetroFramework.Forms;
namespace Electronic
{
    public partial class Auth : Form
    {
        DataBase database = new DataBase();
        SqlConnection sqlcon = new SqlConnection();
        DataSet dataset = new DataSet();
        SqlDataAdapter adapter = new SqlDataAdapter();
        SqlCommandBuilder builder = new SqlCommandBuilder();
        
        Director director = new Director();
        Manager manager = new Manager();
        Client client = new Client();
        Accountant accountant = new Accountant();
        PriceList pricelist = new PriceList();
        bool Open = false;
        public Auth()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ShowIcon = false;
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(database.connectDataBase()))
                {
                    
                    adapter = new SqlDataAdapter("Select Логин,Пароль,ID_Должности,Имя,Отчество from Сотрудники", connection);
                    builder = new SqlCommandBuilder(adapter);
                    dataset = new DataSet();
                    adapter.Fill(dataset);
                    for (int i = 0; i < dataset.Tables[0].Rows.Count; i++)
                    {
                        if (textBox1.Text == dataset.Tables[0].Rows[i][0].ToString())
                        {
                            if (textBox2.Text == dataset.Tables[0].Rows[i][1].ToString())
                            {
                                string greeting = "Здравствуйте, " + dataset.Tables[0].Rows[i][3].ToString() + " " + dataset.Tables[0].Rows[i][4].ToString() + "!";
                                MessageBox.Show(greeting, "Авторизация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                switch (dataset.Tables[0].Rows[i][2].ToString())
                                {
                                    case "1": director.label1.Text = "Здравствуйте, " + dataset.Tables[0].Rows[i][3].ToString() + "!"; director.Show();break;
                                    case "2": manager.label2.Text = "Здравствуйте, " + dataset.Tables[0].Rows[i][3].ToString() + "!"; manager.Show(); break;
                                    case "4": accountant.label1.Text = "Здравствуйте, " + dataset.Tables[0].Rows[i][3].ToString() + "!"; accountant.Show(); break;
                                }
                                this.Hide();
                                Open = true;
                                break;
                            }
                        }
                    }
                    adapter = new SqlDataAdapter("select * from Клиенты", connection);
                    builder = new SqlCommandBuilder(adapter);
                    dataset = new DataSet();
                    adapter.Fill(dataset);
                    for (int i = 0; i < dataset.Tables[0].Rows.Count; i++)
                    {
                        if (textBox1.Text == dataset.Tables[0].Rows[i][4].ToString())
                        {
                            if (textBox2.Text == dataset.Tables[0].Rows[i][5].ToString())
                            {

                                client.ID_Client = dataset.Tables[0].Rows[i][0].ToString();
                                pricelist.label2.Text = "sdassa";
                                string greeting = "Здравствуйте, " + dataset.Tables[0].Rows[i][2].ToString() + " " + dataset.Tables[0].Rows[i][3].ToString() + "!";
                                MessageBox.Show(greeting, "Авторизация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                client.label1.Text = "Здравствуйте, " + dataset.Tables[0].Rows[i][2].ToString() + " " + dataset.Tables[0].Rows[i][3].ToString(); ;
                                client.Show();
                                this.Hide();
                                Open = true;
                                break;
                            }
                        }
                    }
                    if (Open == false)
                    {
                        MessageBox.Show("Неверный логин или пароль", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);

            }
        }

        private void label5_Click(object sender, EventArgs e)
        {
            Registration reg = new Registration();
            this.Hide();
            reg.Show();
        }

        private void button1_MouseMove(object sender, MouseEventArgs e)
        {
            button1.BackColor = Color.Black;
            button1.ForeColor = Color.White;
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            button1.BackColor = Color.White;
            button1.ForeColor = Color.Black;
        }

        private void metroCheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (textBox2.UseSystemPasswordChar == true)
            {
                textBox2.UseSystemPasswordChar = false;
            }
            else
            {
                textBox2.UseSystemPasswordChar = true;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            client.Show();
        }

        private void Auth_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}

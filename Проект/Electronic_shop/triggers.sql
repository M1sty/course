use Electronic_shop
go

create trigger setPrice 
on  ������_��_������������
for insert
as
declare @idService int,@idRequest int
select @idRequest = (select ID_������ from inserted)
select @idService = (select ID_������ from inserted)
begin
	update ������_��_������������
	set �����_��_������ = (select ���� from ������ where ID_������ = @idService)
	where ID_������ = @idRequest
end
go

create trigger checkEmployee 
on ������_��_������������ 
for update
as
declare @idEmployee int
select @idEmployee = (select ID_���������� from inserted)
begin
	if  ((select count(*) from ������_��_������������ where ID_���������� = @idEmployee and ������ =2 ) > 1)
		begin
			rollback transaction
			raiserror('������ ��������� ��� �����',0,1)
		end
	else
		begin
			print '������ ��������� ����������'
		end
end
go

DBCC CHECKIDENT (������_��_������������,RESEED,5)
go

create trigger chechOnDate
on ������
for insert 
as
declare @date date,@idReport int,@profit money,@countWorks int
select @profit = (select sum(�����_��_������) from ������_��_������������  where   datediff(month,����_������,getdate()) = 0)
select @countWorks = (select count(ID_������) from ������_��_������������  where   datediff(month,����_������,getdate()) = 0 and ������ = 3)
select @idReport = (select ID_������ from inserted)
select @date = (select ����_������ from inserted)
begin
	if (@date = EOMONTH(getdate()) )
		begin
			 update ������
			 set ����������_�����������_����� = @countWorks
			 where ID_������ = @idReport
			 update ������
			 set ������� = @profit
			 where ID_������ = @idReport
			 raiserror('����� ������� ��������',0,1)
		end
	else 
		begin
			rollback transaction
			raiserror('����� ������������� ������ � ����� ������',0,1)
		end
end
go